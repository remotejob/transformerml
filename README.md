# transformerml

export KAGGLE_CONFIG_DIR=/exwindoz/home/juno/kaggleapi

kaggle kernels pull -k remotejob/transformerml -p train -m


kaggle kernels push -p train/
kaggle kernels status remotejob/transformerml 
kaggle kernels output remotejob/transformerml -p output/
cp output/model0.pt data/model0.pt
cd data
tar cfv libs.tar libs/
cd -
kaggle datasets version -p data/ -m "Updated data 0"
kaggle datasets version -p dataoldbot/ -m "Updated data oldbot"
kaggle datasets status remotejob/transformerdata


